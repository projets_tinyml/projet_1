# Projet_1

Objectif : Répondre à trois questions en faisant le signe yes (V) ou no (X) avec le microcontrôleur dans la main. 

Etape 1 : Récupérer les coordonnées aX aY aY gX gY gZ avec IMU_Capture.ino. Pour récupérer ces données on faisait les deux gestes plusieurs fois(je l'ai fait 10 fois). On stocke ces valeurs dans un fichier csv. 

Etape 2 : Après avoir fait les fichiers yes.csv et no.csv on bascule sur GoogleColab afin d'entrainer notre modèle et récuperer le fichier model.h. 

Etape 3: On retourne maintenant sur arduino afin d'utiliser notre fichier model.h. Avec le code IMU_Classifer on pose des questions à l'utilisateur puis on attend qu'il réponde avec un des deux signes.

Ce qu'on obtient : 


